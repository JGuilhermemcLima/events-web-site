const express = require("express")
const router = express.Router()
const scheduleRoutes = require("./routes/events")
const categoryRoutes = require("./routes/category")
const tagsRoutes = require("./routes/tags")

router.use('/events', scheduleRoutes)
router.use('/category',categoryRoutes)
router.use('/tags', tagsRoutes)

module.exports = router