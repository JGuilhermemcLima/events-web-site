module.exports = class category{
    constructor(id,title){
        this.id = id
        this.title = title
    }
    static tableName = 'category'
    static fields = `title`
    toInsertDbValues() {
        return `'${this.title}'`
    }

    toUpdateDbValues() {
        return `title = '${this.title}'`
    }
}
