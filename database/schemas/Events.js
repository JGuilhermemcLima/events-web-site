module.exports = class events {
    constructor(id, title, description, email) {
        this.id = id
        this.title = title
        this.description = description
        this.email = email
    }

    static tableName = 'events'
    static fields = `title, description, email`

    toInsertDbValues() {
        return `'${this.title}', '${this.description}', '${this.email}'`
    }

    toUpdateDbValues() {
        return `title='${this.title}', description='${this.description}', email='${this.email}'`
    }
}