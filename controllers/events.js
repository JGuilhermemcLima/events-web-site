const EventsRepository = require('../repositories/events')
const EventsModel = require('../models/Events')
const EventsSchema = require('../database/schemas/Events')
const events = require('../database/schemas/Events')
module.exports = {
    list: async () => {
        try {
            const data = await EventsRepository.list()
            const parsed = []

            data.forEach(events => {
                parsed.push(new EventsModel(
                events.id,
                events.title,
                events.description,
                events.create_date,
                events.end_date,
                events.upload_date,
                events.published_date,
                events.email,
                events.website,
                events.phone,
                events.address,
                events.location_name,
                ))
            })
            return { status: 200, data: parsed }
        }
        catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    create: async data => {
        try {
            const events = new EventsSchema(null, data.title, data.description, data.email)
            await EventsRepository.create(events.toInsertDbValues())
            return { status: 201, data: 'Success creating' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    get: async id => {
        try {
            const events = (await EventsRepository.get(id))[0]
            if (!events)
                return { status: 404, data: 'Not found' }

            const parsed = new EventsModel(
                events.id,
                events.title,
                events.description,
                events.create_date,
                events.end_date,
                events.upload_date,
                events.published_date,
                events.email,
                events.website,
                events.phone,
                events.address,
                events.location_name,
            )

            return { status: 200, data: parsed }
        }
        catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    update: async data => {
        try {
            const events = new EventsSchema(data.id, data.title, data.description, data.email)
            await EventsRepository.update(events.id, events.toUpdateDbValues())
            return { status: 200, data: 'Success updating' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    delete: async id => {
        try {
            await EventsRepository.delete(id)
            return { status: 200, data: 'Success deleting' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    }
}